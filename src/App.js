import avatar from './assets/images/avatar.jpg';
import './App.css';

const dcContainer = {
  width:"800px",
  margin:"0 auto",
  marginTop:"100px",
  border: "1px solid #cacaca",
  paddingBottom:"30px",
  textAlign:"center"
}

const dcImage = {
  width:"100px",
  marginTop:"-50px",
  borderRadius:"50%",
}

function App() {
  return (
    <div style={dcContainer}>
      <img src={avatar} style={dcImage} />
      <p style={{fontSize:"20px"}}>This is one of the best developer blogs on the planet! I read it daily to improve my skills</p>
      <p><b>Tammy Stevens</b> Front End Developer</p>
    </div>
  );
}

export default App;
